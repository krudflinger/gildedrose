products = {}


def default_func(item):
    item._sell_in -= 1
    if item.sell_in > 0:
        item._quality -= 1
    else:
        item._quality = 0


def product_register(name=None):
    def qualifier(qualifier_func):
        products[name] = qualifier_func
        return qualifier_func
    return qualifier


@product_register(name='Sulfuras, Hand of Ragnaros')
def sulfuras_func(item):
    print('"Sulfuras", being a legendary item, never has to be sold or decreases in Quality')
    item._sell_in = None


@product_register(name='Aged Brie')
def agedbrie_func(item):
    item._sell_in -= 1
    if item.sell_in > 0:
        item.quality += 1
    else:
        item.quality = 0


@product_register(name='Backstage passes to a TAFKAL80ETC concert')
def backstage_func(item):
    item._sell_in -= 1
    if item.sell_in < 0:
        item.quality = 0
    elif item.sell_in < 6:
        item.quality += 3
    elif item.sell_in < 11:
        item.quality += 2
    else:
        item.quality += 1


class Item(object):
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        if quality > 0 and quality <= 50:
            self.quality = quality

    def __repr__(self):
        return f'{self.name}, {self.sell_in}, {self.quality}'


class GildedRose(Item):
    def __init__(self, name, sell_in, quality):
        self._name = name
        self._sell_in = sell_in
        self._quality = quality
        super().__init__(name, sell_in, quality)

    @property
    def sell_in(self):
        return self._sell_in

    @sell_in.setter
    def sell_in(self, value):
        self._sell_in = value

    @property
    def quality(self):
        return self._quality

    @quality.setter
    def quality(self, value):
        return self._set_quality(value)

    def _set_quality(self, value):
        if not value:
            pass
        else:
            if value < 0:
                raise ValueError("Quality cannot be negative")
            if value > 50:
                raise ValueError("Quality cannot exceed 50")
            else:
                self._quality = value

    def _update(self):
        if self._name not in products:
            return default_func(self)
        else:
            return products[self._name](self)

    @property
    def update_quality(self):
        return self._update()
