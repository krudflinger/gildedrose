import unittest
import pytest
import main
from main import GildedRose, Item
examples = (
    ("item_name", "initial_quality", "initial_sellin",
     "updated_quality", "updated_sellin", "comment"),
    (
        ("foo", 50, 0, 0, -1, "fake"),
        ("foo", 0, 0, 0, -1, "fake"),
        ("foo", -1, 0, 0, -1, "fake"),
        ("foo", 51, 0, 0, -1, "fake"),
        ("foo", 30, 10, 29, 9, "fake"),
        ("Aged Brie", 0, 0, 0, -1, "Old"),
        ("Sulfuras, Hand of Ragnaros", 0, 0, 0, None, "Nice"),
        ("Backstage passes to a TAFKAL80ETC concert", 0, 0, 0, -1, "Pass"),
        ("Backstage passes to a TAFKAL80ETC concert", 49, 15, 50, 14, "Pass"),
        ("Aged Brie", 0, 0, 0, -1, "Old"),
        ("Aged Brie", 20, -1, 20, -2, "Old"),
        ("Aged Brie", 20, 23, 21, 22, "Old"),
        ("Sulfuras, Hand of Ragnaros", 1, 0, 1, None, "Nice"),
        ("Backstage passes to a TAFKAL80ETC concert", 0, 0, 0, -1, "Pass"),
        ("Backstage passes to a TAFKAL80ETC concert", 47, 6, 50, 5, "Pass"),
        ("Backstage passes to a TAFKAL80ETC concert", 48, 11, 50, 10, "Pass"),
        ("Sulfuras, Hand of Ragnaros", 50, None, 50, None, "Pass"),
    )
)


@pytest.mark.parametrize(*examples)
def test_update_quality(item_name, initial_quality, initial_sellin, updated_quality, updated_sellin, comment):
    gilded_rose = GildedRose(item_name, initial_sellin, initial_quality)
    gilded_rose.update_quality
    assert gilded_rose.quality == updated_quality
    assert gilded_rose.sell_in == updated_sellin
    